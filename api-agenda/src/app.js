'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

// Conecta ao banco
mongoose.connect('mongodb://felipe:Mudar123@ds038739.mlab.com:38739/dbapp');

// Carrega os Models
const Contato = require('./models/contato');

// Carrega as rotas
const indexRoute = require('./routes/index-route');
const contatoRoute = require('./routes/contato-route');

// Configuração do Cabeçalho / Header
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRoute);
app.use('/contatos', contatoRoute);

module.exports = app;
