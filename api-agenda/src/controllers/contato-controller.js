'use strict'

const mongoose = require('mongoose');
const Contato = mongoose.model('Contato');

exports.get = (req, res, next) => {
  Contato.find({}, 'nome email telefone').then(data => {
    res.status(200).send(data);
  }).catch(e => {
    res.status(400).send(e);
  });
};

exports.getById = (req, res, next) => {
  Contato.findById(req.params.id)
    .then(data => {
      res.status(200).send(data);
    }).catch(e => {
      res.status(400).send(e);
    });
};

exports.post = (req, res, next) => {
  var contato = new Contato(req.body);
  contato.save().then(x => {
    res.status(201).send({ message: 'Contato cadastrado com sucesso!' });
  }).catch(e => {
    res.status(400).send({ message: 'Falha ao cadastrar o contato', data: e });
  });
};

exports.put = (req, res, next) => {
  Contato
    .findByIdAndUpdate(req.params.id, {
      $set: {
        nome: req.body.nome,
        email: req.body.email,
        telefone: req.body.telefone
      }
    }).then(x => {
      res.status(201).send({
        message: 'Contato atualizado com sucesso!'
      });
    }).catch(e => {
      res.status(400).send({
        message: 'Falha ao atualizar contato',
        data: e
      });
    });
};

exports.delete = (req, res, next) => {
  Contato
    .findOneAndRemove(req.body.id)
    .then(x => {
      res.status(200).send({
        message: 'Contato removido com sucesso!'
      });
    }).catch(e => {
      res.status(400).send({
        message: 'Falha ao remover contato',
        data: e
      });
    });
};
