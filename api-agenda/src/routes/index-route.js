'use strict'

const express = require('express');
const router = express.Router();

const route = router.get('/', (req, res, next) => {
  res.status(200).send({
    nome: "Moises",
    email: "moises@innovareti.com.br",
    telefone: "(011) 91234-4321"
  });
});

module.exports = router;
