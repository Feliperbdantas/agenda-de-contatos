import Contato from './components/contato/Contato.vue';
import Agenda from './components/agenda/Agenda.vue';

export const routes = [
    { path: '', name: 'agenda', component: Agenda, titulo: 'Agenda', menu: true},
    { path: '/contato', name: 'cadastroContato', component: Contato, titulo: 'Cadastrar Contato', menu: true},
    { path: '/contato/:id', name: 'alteraContato', component: Contato, titulo: 'Alterar Contato', menu: false},
    { path: '*', component: Agenda, menu: false}
];