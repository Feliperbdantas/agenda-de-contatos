export default class ContatoService {
    constructor(resource){
      this._resource = resource('contatos{/id}');
    }

    lista(){
      return this._resource
        .query()
        .then(res => res.json(), err => {
          console.log(err);
          throw new Error('Não foi possível obter os contatos');
        });
    }
    cadastra(contato){
      if(contato._id) return this._resource.update({ id: contato._id }, contato);
      else return this._resource.save(contato);
    }
    apaga(id){
      return this._resource
        .delete({ id })
        .then(null, err => {
          console.log(err);
          throw new Error('Não foi possível remover o contato');
        });
    }
    busca(id){
      return this._resource
        .get({ id })
        .then(res => res.json());
    }
}